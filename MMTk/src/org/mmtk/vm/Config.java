/*
 *  This file is part of the Jikes RVM project (http://jikesrvm.org).
 *
 *  This file is licensed to You under the Eclipse Public License (EPL);
 *  You may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at
 *
 *      http://www.opensource.org/licenses/eclipse-1.0.php
 *
 *  See the COPYRIGHT.txt file distributed with this work for information
 *  regarding copyright ownership.
 */
package org.mmtk.vm;

import org.mmtk.utility.Log;
import org.mmtk.utility.statistics.Xml;

public class Config {
  /** The name of the active plan */
  private final String ACTIVE_PLAN;

  /** Mark bit in the header or on the side ? */
  public final boolean HEADER_MARK_BITS;
  
  public final int YIELDPOINT_IMPL;
  
  public static final int USE_CHECKING_YP = 0;
  public static final int USE_PAGE_PROTECTION_READ_YP = 1;
  public static final int USE_PAGE_PROTECTION_WRITE_YP = 2;
  public static final int USE_CODE_PATCHING_INT3_YP = 3;
  public static final int USE_CODE_PATCHING_CALL_YP = 4;
  
  public static final int USE_CODE_PATCHING_CALL_LOCAL_YP = 5;
  
  public static final int USE_GLOBAL_CHECKING_YP = 10;
  public static final int USE_GLOBAL_PAGE_PROTECTION_READ_YP = 11;
  public static final int USE_GLOBAL_PAGE_PROTECTION_WRITE_YP = 12;
  
  public static final int USE_NO_YP = 20;
  public static final int USE_NOP1_YP = 21;
  public static final int USE_NOP4_YP = 24;
  public static final int USE_NOP6_YP = 26;
  public static final int USE_NONOP_YP = 30;
  
  public final boolean ENABLE_YP_STAT;

  Config(BuildTimeConfig config) {
    ACTIVE_PLAN            = config.getPlanName();
    HEADER_MARK_BITS        = config.getBooleanProperty("mmtk.headerMarkBit",true);
    
    if (config.getBooleanProperty("mmtk.use_checking_yp", false)) {
    	YIELDPOINT_IMPL = USE_CHECKING_YP;
    } else if (config.getBooleanProperty("mmtk.use_page_protection_read_yp", false)) {
    	YIELDPOINT_IMPL = USE_PAGE_PROTECTION_READ_YP;
    } else if (config.getBooleanProperty("mmtk.use_page_protection_write_yp", false)) {
    	YIELDPOINT_IMPL = USE_PAGE_PROTECTION_WRITE_YP;
    } else if (config.getBooleanProperty("mmtk.use_code_patching_int3_yp", false)) {
    	YIELDPOINT_IMPL = USE_CODE_PATCHING_INT3_YP;
    } else if (config.getBooleanProperty("mmtk.use_code_patching_call_yp", false)) {
    	YIELDPOINT_IMPL = USE_CODE_PATCHING_CALL_YP;
    } else if (config.getBooleanProperty("mmtk.use_code_patching_call_local_yp", false)) {
    	YIELDPOINT_IMPL = USE_CODE_PATCHING_CALL_LOCAL_YP;
    }
    else if (config.getBooleanProperty("mmtk.use_no_yp", false)) {
    	YIELDPOINT_IMPL = USE_NO_YP;
    } else if (config.getBooleanProperty("mmtk.use_global_checking_yp", false)) {
    	YIELDPOINT_IMPL = USE_GLOBAL_CHECKING_YP;
    } else if (config.getBooleanProperty("mmtk.use_global_page_protection_read_yp", false)) {
    	YIELDPOINT_IMPL = USE_GLOBAL_PAGE_PROTECTION_READ_YP;
    } else if (config.getBooleanProperty("mmtk.use_global_page_protection_write_yp", false)) {
    	YIELDPOINT_IMPL = USE_GLOBAL_PAGE_PROTECTION_WRITE_YP;
    } else if (config.getBooleanProperty("mmtk.use_nop1_yp", false)) {
    	YIELDPOINT_IMPL = USE_NOP1_YP;
    } else if (config.getBooleanProperty("mmtk.use_nop4_yp", false)) {
    	YIELDPOINT_IMPL = USE_NOP4_YP;
    } else if (config.getBooleanProperty("mmtk.use_nonop_yp", false)) {
    	YIELDPOINT_IMPL = USE_NONOP_YP;
    }
    else if (config.getBooleanProperty("mmtk.use_nop6_yp", false)) {
    	YIELDPOINT_IMPL = USE_NOP6_YP;
    }
    
    else {
    	YIELDPOINT_IMPL = USE_CHECKING_YP;
    }
    
    ENABLE_YP_STAT = config.getBooleanProperty("mmtk.enable_yp_stat", false);
  }

  public void printConfig() {
    Log.writeln("================ MMTk Configuration ================");
    Log.write("plan = "); Log.writeln(ACTIVE_PLAN);
    Log.write("HEADER_MARK_BITS = ");  Log.writeln(HEADER_MARK_BITS);
    Log.writeln("====================================================");
  }

  public void printConfigXml() {
    Log.writeln("<config>");
    Xml.configItem("plan",ACTIVE_PLAN);
    Xml.configItem("header-mark-bit",HEADER_MARK_BITS);
    Log.writeln("</config>");
  }
}
