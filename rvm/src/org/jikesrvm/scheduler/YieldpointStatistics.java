package org.jikesrvm.scheduler;

import org.jikesrvm.VM;
import org.jikesrvm.adaptive.controller.Controller;
import org.jikesrvm.adaptive.database.AOSDatabase;
import org.jikesrvm.adaptive.measurements.instrumentation.Instrumentation;
import org.jikesrvm.classloader.NormalMethod;
import org.jikesrvm.classloader.RVMMethod;
import org.jikesrvm.compilers.common.CompiledMethod;
import org.jikesrvm.compilers.common.CompiledMethods;
import org.jikesrvm.compilers.opt.controlflow.YieldPoints;
import org.jikesrvm.mm.mmtk.SynchronizedCounter;
import org.jikesrvm.runtime.SysCall;
import org.mmtk.utility.statistics.LongCounter;
import org.mmtk.utility.statistics.Timer;
import org.vmmagic.pragma.Entrypoint;
import org.vmmagic.pragma.Inline;
import org.vmmagic.pragma.Interruptible;
import org.vmmagic.pragma.Uninterruptible;

@Uninterruptible
public class YieldpointStatistics {
	// use -X:aos:insert_yieldpoint_counters with this flag
	public static final boolean ENABLE_YP_COUNT = true;
	
	// how much time patching takes
	public static double patchingCost = 0;

	// how many yieldpoint are executed in baseline code
	public static SynchronizedCounter baselineYieldpointExecuted = new SynchronizedCounter();
	
	// how many yieldpoints taken in total
	public static SynchronizedCounter yieldpointTaken = new SynchronizedCounter();
	
	public static long yieldpointOptInsertedForPrologue = 0;
	public static long yieldpointOptInsertedForEpilogue = 0;
	public static long yieldpointOptInsertedForBackedge = 0;
	public static long yieldpointBaselineInserted = 0;
	
	public static long yieldpointOptInsertedForVM = 0;
	public static long yieldpointOptInsertedForApp = 0;

	public static long gcRequestBlockTime;

	public static long yieldpointTurnedOnLatency = 0;
	public static long allThreadBlockedLatency = 0;

	// these two work together to get a mutator time (starts when all mutators
	// resume or probe starts, ends when all mutators end or probe ends)
	public static long mutatorStartTimeMillis;
	public static long mutatorTimeWithoutBlockingCost = 0;
	
	public static int maxYieldpointsPatched = 0;

	@Interruptible
	public static void reset() {
		if (ENABLE_YP_COUNT) {
			yieldpointTaken.reset();
			baselineYieldpointExecuted.reset();
						
//			yieldpointOptInsertedForPrologue.reset();
//			yieldpointOptInsertedForBackedge.reset();
			
			AOSDatabase.yieldpointCounterData.reset();
			Instrumentation.enableInstrumentation();
		}

		gcRequestBlockTime = 0;
		yieldpointTurnedOnLatency = 0;
		allThreadBlockedLatency = 0;

		mutatorTimeWithoutBlockingCost = 0;
		mutatorStartTimeMillis = SysCall.sysCall.sysCurrentTimeMillis();
		
		maxYieldpointsPatched = 0;
		patchingCost = 0;
	}

	@Interruptible
	public static void stopData() {
		mutatorTimeWithoutBlockingCost += SysCall.sysCall.sysCurrentTimeMillis() - mutatorStartTimeMillis;
		
		if (ENABLE_YP_COUNT)
			Instrumentation.disableInstrumentation();
	}
	
	public static void yieldpointInserted(int position, NormalMethod method) {
		switch(position) {
		case RVMThread.PROLOGUE: yieldpointOptInsertedForPrologue++; break;
		case RVMThread.EPILOGUE: yieldpointOptInsertedForEpilogue++; break;
		case RVMThread.BACKEDGE: yieldpointOptInsertedForBackedge++; break;
		}
		
		if (method.isInVM())
			yieldpointOptInsertedForVM++;
		else yieldpointOptInsertedForApp++;
	}
	
	public static void yieldpointsPatched(int patched) {
		if (patched > maxYieldpointsPatched)
			maxYieldpointsPatched = patched;
	}
	
	public static void timeUsedOnPatching(long time) {
		patchingCost += time;
	}
	
	public static void executeBaselineYP() {
		baselineYieldpointExecuted.increment();
	}

	@Inline
	public static void takeYieldpoint() {
		yieldpointTaken.increment();
	}

	@Interruptible
	public static void probeStart() {
		reset();
	}

	@Interruptible
	public static void probeEnd() {
		stopData();
		
		if (ENABLE_YP_COUNT) {
//			showYieldpointExecutedDist();
		}
		
		printStatistics();
	}
	
	@Interruptible
	public static void showYieldpointExecutedDist() {
		VM.sysWriteln("reading in yp data");
		double[] count = AOSDatabase.yieldpointCounterData.reportYieldpointsOfID();
		
		VM.sysWriteln("sorting results");
		// bubble sort
		boolean flag = true;
		double tmp;
		
		while (flag) {
			flag = false;
			for (int i = 0; i < count.length - 1; i++) {
				if (count[i] < count[i+1]) {
					tmp = count[i];
					count[i] = count[i+1];
					count[i+1] = tmp;
					flag = true;
				}
			}
		}
		
		double total = 0;
		for (int i = 0; i < count.length; i++)
			total += count[i];
		
		if (total == 0)
			VM.sysFail("failed to collect result");
		
		double accumulatePercentage = 0;
		for (int i = 0; i < count.length; i++) {
			accumulatePercentage += (count[i] / total);
			VM.sysWrite(i, ",");
			VM.sysWriteln(accumulatePercentage);
			
			if (accumulatePercentage > 0.999) {
				return;
			}
		}
	}
	
	public static void printStatisticsForBootImage() {
		VM.sysWriteln("============================");
		VM.sysWriteln("yieldpoint inserted for VM: ", yieldpointOptInsertedForVM);
		VM.sysWriteln("yieldpoint inserted for APP:", yieldpointOptInsertedForApp);
	}

	public static void printStatistics() {
		VM.sysWriteln("============================ Tabulate Statistics ============================");
		printColumn("yieldpointTurnedOnLatencySum");
		printColumn("allThreadsBlockedLatencySum");
		printColumn("mutatorTimeWithoutBlockingCost");
		printColumn("maxYieldpointsPatched");
		printColumn("patchingCost");
		if (ENABLE_YP_COUNT) {
			printColumn("yieldpointTaken");
			printColumn("yieldpointExecutedFromPrologue");
			printColumn("yieldpointExecutedFromEpilogue");
			printColumn("yieldpointExecutedFromBackedge");
			printColumn("yieldpointExecutedFromBaseline");
			printColumn("yieldpointExecutedFromOpt");
			printColumn("yieldpointExecutedFromJNI");
			printColumn("yieldpointExecutedFromTrap");
			printColumn("yieldpointInsertedForPrologue");
			printColumn("yieldpointInsertedForEpilogue");
			printColumn("yieldpointInsertedForBackedge");
			printColumn("yieldpointInsertedForBaseline");
			printColumn("yieldpointInsertedForVM");
			printColumn("yieldpointInsertedForApp");
			printColumn("totalInstructionsInVM");
			printColumn("totalInstructionsInApp");
		}
		printNewLine();
		printColumn(yieldpointTurnedOnLatency);
		printColumn(allThreadBlockedLatency);
		printColumn(mutatorTimeWithoutBlockingCost);
		printColumn(maxYieldpointsPatched);
		printColumn(patchingCost);
		if (ENABLE_YP_COUNT) {
			printColumn(yieldpointTaken.peek());
			printColumn(AOSDatabase.yieldpointCounterData.reportPrologueYieldpoints());
			printColumn(AOSDatabase.yieldpointCounterData.reportEpilogueYieldpoints());
			printColumn(AOSDatabase.yieldpointCounterData.reportBackEdgeYieldpoints());
			printColumn((double)baselineYieldpointExecuted.peek());
			printColumn(AOSDatabase.yieldpointCounterData.reportOptYieldpoints());
			printColumn(AOSDatabase.yieldpointCounterData.reportJNIYieldpoints());
			printColumn(AOSDatabase.yieldpointCounterData.reportTrapYieldpoints());
			printColumn((double)yieldpointOptInsertedForPrologue);
			printColumn((double)yieldpointOptInsertedForEpilogue);
			printColumn((double)yieldpointOptInsertedForBackedge);
			printColumn((double)yieldpointBaselineInserted);
			printColumn((double)yieldpointOptInsertedForVM);
			printColumn((double)yieldpointOptInsertedForApp);
			printColumn(countInstructionsForVM());
			printColumn(countInstructionsForApp());
		}
		printNewLine();
		VM.sysWriteln("------------------------------ End Tabulate Statistics -----------------------------");
	}

	private static double countInstructionsForApp() {
		double ret = 0;
		
		for (int i = 0, n = CompiledMethods.numCompiledMethods(); i < n; i ++) {
			  CompiledMethod cm = CompiledMethods.getCompiledMethodUnchecked(i);
			  
			  if (cm != null && cm.isCompiled() && cm.getCompilerType() == CompiledMethod.OPT) {
//				  cm.size();
				  if (!cm.getMethod().isInVM())
					  ret += cm.numberOfInstructions();
			  }
		}
		
		return ret;
	}

	private static double countInstructionsForVM() {
		double ret = 0;
		
		for (int i = 0, n = CompiledMethods.numCompiledMethods(); i < n; i ++) {
			  CompiledMethod cm = CompiledMethods.getCompiledMethodUnchecked(i);
			  
			  if (cm != null && cm.isCompiled() && cm.getCompilerType() == CompiledMethod.OPT) {
				  if (cm.getMethod().isInVM())
					  ret += cm.numberOfInstructions();
			  }
		}
		
		return ret;
	}

	private static void printColumn(String s) {
		VM.sysWrite(s);
		VM.sysWrite("\t");
	}

	private static void printColumn(int i) {
		VM.sysWrite(i);
		VM.sysWrite("\t");
	}

	private static void printColumn(double d) {
		VM.sysWrite(d);
		VM.sysWrite("\t");
	}

	private static void printNewLine() {
		VM.sysWriteln();
	}
}
